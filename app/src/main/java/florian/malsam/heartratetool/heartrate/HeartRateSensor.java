package florian.malsam.heartratetool.heartrate;

import android.os.Parcelable;

import java.util.Objects;


public interface HeartRateSensor extends Parcelable{

    String getAdress();
    String getName();
    void setAdress(String s);
    void setName(String s);
    boolean equals(Object v);
}
