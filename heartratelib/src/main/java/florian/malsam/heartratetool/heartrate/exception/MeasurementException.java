package florian.malsam.heartratetool.heartrate.exception;

/**
 * Created by flo on 04.01.2018.
 */

public class MeasurementException extends HeartRateException {

    public MeasurementException(String s)
    {
        super(s);
    }

}
