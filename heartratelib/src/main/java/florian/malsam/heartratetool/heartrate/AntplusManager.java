package florian.malsam.heartratetool.heartrate;

import android.app.Activity;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.dsi.ant.plugins.antplus.pcc.AntPlusHeartRatePcc;
import com.dsi.ant.plugins.antplus.pcc.defines.DeviceState;
import com.dsi.ant.plugins.antplus.pcc.defines.EventFlag;
import com.dsi.ant.plugins.antplus.pcc.defines.RequestAccessResult;
import com.dsi.ant.plugins.antplus.pccbase.AntPluginPcc;
import com.dsi.ant.plugins.antplus.pccbase.AsyncScanController;
import com.dsi.ant.plugins.antplus.pccbase.PccReleaseHandle;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.EnumSet;
import java.util.List;

import florian.malsam.heartratetool.heartrate.exception.HeartRateException;

/**
 * Created by flo on 18.07.2017.
 */

public class AntplusManager extends HeartRateManager {


    private Activity mParent = null;


    AntPlusHeartRatePcc hrPcc = null;
    protected PccReleaseHandle<AntPlusHeartRatePcc> releaseHandle = null;
    AsyncScanController<AntPlusHeartRatePcc> hrScanCtrl;
    AntPlusHeartRatePcc heartRatePcc;

    //devicehelpers
    ArrayList<AsyncScanController.AsyncScanResultDeviceInfo> mAlreadyConnectedDeviceInfos;
    ArrayList<AsyncScanController.AsyncScanResultDeviceInfo> mScannedDeviceInfos;
    ArrayAdapter<String> adapter_devNameList;
    ArrayAdapter<String> adapter_connDevNameList;

    boolean measuring = false;

    List<Integer> heartRateValues;


    public AntplusManager(Activity parent, HeartRateCallbacks callbacks)
    {
        super(callbacks);
        mParent = parent;

    }

    /* initialize BLE and get BT Manager & Adapter */
    public void initialize() {

        mAlreadyConnectedDeviceInfos = new ArrayList<AsyncScanController.AsyncScanResultDeviceInfo>();
        mScannedDeviceInfos = new ArrayList<AsyncScanController.AsyncScanResultDeviceInfo>();

    }


    @Override
    public void startScanning() {

        requestAccessToPcc();


    }



    @Override
    public void stopScanning() {

        //hrScanCtrl.closeScanController();

    }

    @Override
    public void connect(HeartRateSensor s) {

        AntplusSensor as = (AntplusSensor)s;
        callBacks.connectionStateChanged(s, CONNECTION.CONNECTING);
        requestConnectToResult((AsyncScanController.AsyncScanResultDeviceInfo)as.getRealDevice());

    }



    @Override
    public void close() {

        if(hrScanCtrl != null)
        {
            hrScanCtrl.closeScanController();
            hrScanCtrl = null;
        }

    }
    boolean subscribed = false;
    @Override
    public void startMeasurement() {
        if(!subscribed)
            subscribeToHrEvents();
        measuring = true;

    }


    @Override
    public HeartRateDataSet stopMeasurement() {
        measuring = false;
        hrPcc.subscribeHeartRateDataEvent(null);

        return getHeartRateDataSet();
    }

    @Override
    public void connect(final String s) throws HeartRateException {

        final HeartRateSensor sensor;
        final int address;
        try{
            address = Integer.valueOf(s);
        }
        catch(Exception e)
        {
            throw new HeartRateException("Can not connect, address invalid or null");
        }


        PccReleaseHandle pccReleaseHandle = AntPlusHeartRatePcc.requestAccess(mParent, address, 0, new AntPluginPcc.IPluginAccessResultReceiver<AntPlusHeartRatePcc>()
        {
            @Override
            public void onResultReceived(AntPlusHeartRatePcc result,
                                         RequestAccessResult resultCode, DeviceState initialDeviceState)
            {

                if(result != null)
                {
                    AntplusSensor sensor = new AntplusSensor(result.getDeviceName());
                    sensor.setAdress(String.valueOf(result.getAntDeviceNumber()));


                    if(resultCode == RequestAccessResult.SEARCH_TIMEOUT)
                    {
                        //On a connection timeout the scan automatically resumes, so we inform the user, and go back to scanning
                        //connection failed
                        callBacks.connectionStateChanged(sensor, CONNECTION.DISCONNECTED);
                    }
                    else
                    {
                        //Otherwise the results, including SUCCESS, behave the same as
                        callBacks.connectionStateChanged(sensor, CONNECTION.CONNECTED);
                        //base_IPluginAccessResultReceiver.onResultReceived(result, resultCode, initialDeviceState);
                        hrScanCtrl = null;
                        heartRatePcc = result;
                        hrPcc = result;
                    }
                }

            }
        }, base_IDeviceStateChangeReceiver);





    }


    protected void requestAccessToPcc()
    {

        if(hrScanCtrl != null)
        {
            hrScanCtrl.closeScanController();
            hrScanCtrl = null;
        }

        hrScanCtrl = AntPlusHeartRatePcc.requestAsyncScanController(mParent, 0,
                new AsyncScanController.IAsyncScanResultReceiver()
                {
                    @Override
                    public void onSearchStopped(RequestAccessResult reasonStopped)
                    {
                        //The triggers calling this function use the same codes and require the same actions as those received by the standard access result receiver
                        base_IPluginAccessResultReceiver.onResultReceived(null, reasonStopped, DeviceState.DEAD);
                    }

                    @Override
                    public void onSearchResult(final AsyncScanController.AsyncScanResultDeviceInfo deviceFound)
                    {
                        Log.d("1337gs", "searchresult adress: "+deviceFound.getAntDeviceNumber());
                        AntplusSensor gd = new AntplusSensor(deviceFound);
                        gd.setName(deviceFound.getDeviceDisplayName());
                        callBacks.deviceFound(gd);
                        if(!deviceMap.containsKey(gd.getAdress()))
                            deviceMap.put(gd.getAdress(), gd);
                        else
                            Log.d("1337gs", "already contains");


                        //We split up devices already connected to the plugin from un-connected devices to make this information more visible to the user,
                        //since the user most likely wants to be aware of which device they are already using in another app

                    }
                });


    }

    protected void requestConnectToResult(final AsyncScanController.AsyncScanResultDeviceInfo asyncScanResultDeviceInfo)
    {




        //Inform the user we are connecting
        releaseHandle = hrScanCtrl.requestDeviceAccess(asyncScanResultDeviceInfo,
                        new AntPluginPcc.IPluginAccessResultReceiver<AntPlusHeartRatePcc>()
                        {
                            @Override
                            public void onResultReceived(AntPlusHeartRatePcc result,
                                                         RequestAccessResult resultCode, DeviceState initialDeviceState)
                            {
                                if(resultCode == RequestAccessResult.SEARCH_TIMEOUT)
                                {
                                    //On a connection timeout the scan automatically resumes, so we inform the user, and go back to scanning
                                   //connection failed
                                    callBacks.connectionStateChanged(new AntplusSensor(asyncScanResultDeviceInfo), CONNECTION.DISCONNECTED);
                                }
                                else
                                {
                                    //Otherwise the results, including SUCCESS, behave the same as
                                    callBacks.connectionStateChanged(new AntplusSensor(asyncScanResultDeviceInfo), CONNECTION.CONNECTED);
                                    base_IPluginAccessResultReceiver.onResultReceived(result, resultCode, initialDeviceState);
                                    hrScanCtrl = null;
                                    heartRatePcc = result;
                                }
                            }
                        }, base_IDeviceStateChangeReceiver);

    }

    /**
     * Switches the active view to the data display and subscribes to all the data events
     */
    public void subscribeToHrEvents()
    {

        hrPcc.subscribeHeartRateDataEvent(new AntPlusHeartRatePcc.IHeartRateDataReceiver()
        {
            @Override
            public void onNewHeartRateData(final long estTimestamp, EnumSet<EventFlag> eventFlags,
                                           final int computedHeartRate, final long heartBeatCount,
                                           final BigDecimal heartBeatEventTime, final AntPlusHeartRatePcc.DataState dataState)
            {
                // Mark heart rate with asterisk if zero detected
                final String textHeartRate = String.valueOf(computedHeartRate)
                        + ((AntPlusHeartRatePcc.DataState.ZERO_DETECTED.equals(dataState)) ? "*" : "");


                if(timer_started)
                {
                    timer_started = false;
                    new Thread(new Runnable() {

                        public void run() {
                            while (counter > 0 && running) {
                                try {
                                    Thread.sleep(1000);
                                } catch (InterruptedException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }

                                counter--;
                                callBacks.secondDone(counter);
                            }
                            //nur wenn timer abgelaufen
                            if(running)
                            {
                                callBacks.measurementDone(stopMeasurement());
                                running = false;
                            }


                        }

                    }).start();
                }

                if(running && measuring)
                {


                    Date d = new Date();
                    int heartRate = 0;
                    try{
                         heartRate = Math.abs(Integer.parseInt(textHeartRate));
                    }catch(Exception e)
                    {

                    }


                    Log.d("gs1337", heartRate + " s: "+d.getSeconds());

                    HeartRateData tmp = new HeartRateData(heartRate, d.getTime());
                    if(callBacks != null)
                        callBacks.newHeartrateData(tmp);
                    getHeartRateValues().add(tmp);
                }

                Log.d("1337gs", "heartrate: "+textHeartRate);

               /* runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        tv_estTimestamp.setText(String.valueOf(estTimestamp));

                        tv_computedHeartRate.setText(textHeartRate);
                        tv_heartBeatCounter.setText(textHeartBeatCount);
                        tv_heartBeatEventTime.setText(textHeartBeatEventTime);

                        tv_dataStatus.setText(dataState.toString());
                    }
                });*/
            }
        });


    }

    protected AntPluginPcc.IPluginAccessResultReceiver<AntPlusHeartRatePcc> base_IPluginAccessResultReceiver =
            new AntPluginPcc.IPluginAccessResultReceiver<AntPlusHeartRatePcc>()
            {
                //Handle the result, connecting to events on success or reporting failure to user.
                @Override
                public void onResultReceived(AntPlusHeartRatePcc result, RequestAccessResult resultCode,
                                             DeviceState initialDeviceState)
                {
                    Log.d("1337gs", "Connecting...");
                    switch(resultCode)
                    {
                        case SUCCESS:
                            Log.d("1337gs", "success");
                            hrPcc = result;
                            Log.d("1337gs", "connected to "+result.getDeviceName());
                            //subscribeToHrEvents();
                            break;
                        /*case CHANNEL_NOT_AVAILABLE:
                            Log.d("1337gs", "CHANNEL_NOT_AVAILABLE");
                            Toast.makeText(Activity_HeartRateDisplayBase.this, "Channel Not Available", Toast.LENGTH_SHORT).show();
                            tv_status.setText("Error. Do Menu->Reset.");
                            break;
                        case ADAPTER_NOT_DETECTED:
                            Log.d("1337gs", "ADAPTER_NOT_DETECTED");
                            Toast.makeText(Activity_HeartRateDisplayBase.this, "ANT Adapter Not Available. Built-in ANT hardware or external adapter required.", Toast.LENGTH_SHORT).show();
                            tv_status.setText("Error. Do Menu->Reset.");
                            break;
                        case BAD_PARAMS:
                            Log.d("1337gs", "BAD_PARAMS");
                            //Note: Since we compose all the params ourself, we should never see this result
                            Toast.makeText(Activity_HeartRateDisplayBase.this, "Bad request parameters.", Toast.LENGTH_SHORT).show();
                            tv_status.setText("Error. Do Menu->Reset.");
                            break;
                        case OTHER_FAILURE:
                            Log.d("1337gs", "OTHER_FAILURE");
                            Toast.makeText(Activity_HeartRateDisplayBase.this, "RequestAccess failed. See logcat for details.", Toast.LENGTH_SHORT).show();
                            tv_status.setText("Error. Do Menu->Reset.");
                            break;
                        case DEPENDENCY_NOT_INSTALLED:
                            Log.d("1337gs", "DEPENDENCY_NOT_INSTALLED");
                            tv_status.setText("Error. Do Menu->Reset.");
                            AlertDialog.Builder adlgBldr = new AlertDialog.Builder(Activity_HeartRateDisplayBase.this);
                            adlgBldr.setTitle("Missing Dependency");
                            adlgBldr.setMessage("The required service\n\"" + AntPlusHeartRatePcc.getMissingDependencyName() + "\"\n was not found. You need to install the ANT+ Plugins service or you may need to update your existing version if you already have it. Do you want to launch the Play Store to get it?");
                            adlgBldr.setCancelable(true);
                            adlgBldr.setPositiveButton("Go to Store", new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which)
                                {
                                    Intent startStore = null;
                                    startStore = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + AntPlusHeartRatePcc.getMissingDependencyPackageName()));
                                    startStore.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                                    Activity_HeartRateDisplayBase.this.startActivity(startStore);
                                }
                            });
                            adlgBldr.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which)
                                {
                                    dialog.dismiss();
                                }
                            });

                            final AlertDialog waitDialog = adlgBldr.create();
                            waitDialog.show();
                            break;
                        case USER_CANCELLED:
                            Log.d("1337gs", "USER_CANCELLED");
                            tv_status.setText("Cancelled. Do Menu->Reset.");
                            break;
                        case UNRECOGNIZED:
                            Log.d("1337gs", "UNRECOGNIZED");
                            Toast.makeText(Activity_HeartRateDisplayBase.this,
                                    "Failed: UNRECOGNIZED. PluginLib Upgrade Required?",
                                    Toast.LENGTH_SHORT).show();
                            tv_status.setText("Error. Do Menu->Reset.");
                            break;*/
                        default:

                            break;
                    }
                }
            };

    //Receives state changes and shows it on the status display line
    protected AntPluginPcc.IDeviceStateChangeReceiver base_IDeviceStateChangeReceiver =
            new AntPluginPcc.IDeviceStateChangeReceiver()
            {
                @Override
                public void onDeviceStateChange(final DeviceState newDeviceState)
                {
                   /* runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            tv_status.setText(hrPcc.getDeviceName() + ": " + newDeviceState);
                        }
                    });*/


                }
            };


}
