package florian.malsam.heartratetool.heartrate;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.ParcelUuid;
import android.util.Log;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import florian.malsam.heartratetool.heartrate.alternatescanrecord.AlternateScanRecord;
import florian.malsam.heartratetool.heartrate.exception.HeartRateException;
import florian.malsam.heartratetool.heartrate.exception.MeasurementException;

/**
 * Created by flo on 18.07.2017.
 */

public class BleManager extends HeartRateManager {



    private static String HEARTRATE_SERVICE_UUID = "0000180d-0000-1000-8000-00805f9b34fb";
    private static String HEARTRATE_MEASUREMENT_CHARACTERISTIC_UUID = "00002a37-0000-1000-8000-00805f9b34fb";
    private static String CLIENT_CHARACTERISTIC_CONFIGURATION_DESCRIPTOR_UUID = "00002902-0000-1000-8000-00805f9b34fb";
    private Activity mParent = null;


    private BluetoothManager mBluetoothManager = null;
    private BluetoothAdapter mBluetoothAdapter = null;
    private BluetoothGatt mBluetoothGatt = null;
    private BluetoothGattDescriptor mBluetoothGattDescriptor = null;
    private ScanCallback mDeviceFoundCallback = null;
    private BluetoothAdapter.LeScanCallback mLeScanCallback = null;




    public BleManager(Activity parent, HeartRateCallbacks callbacks) throws  HeartRateException
    {
        mParent = parent;
        callBacks = callbacks;
        deviceMap = new HashMap<>();
        initialize();
    }

    public void initialize() throws HeartRateException {
        if (mBluetoothManager == null) {
            mBluetoothManager = (BluetoothManager) mParent.getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager == null) {
                throw new HeartRateException("Could not get Bluetooth Manager");
            }
        }

        if(mBluetoothAdapter == null) mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (mBluetoothAdapter == null) {
            throw new HeartRateException("Could not get Bluetooth Adapter");
        }else
        {
            //enable bluetooth without user interaction
            //mBluetoothAdapter.enable();

        }

        if(!mParent.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)){
            throw new HeartRateException("Hardware does not support BLE");
        }


    }



    @Override
    public void startScanning() {

        if (Build.VERSION.SDK_INT < 21) {
            mBluetoothAdapter.startLeScan(mLeScanCallback  =
                    new BluetoothAdapter.LeScanCallback() {
                        @Override
                        public void onLeScan(final BluetoothDevice device, int rssi,
                                             byte[] scanRecord) {
                            Log.d("mioscanner1337", "old method device found");
                            BleSensor mdv = new BleSensor(device);



                            AlternateScanRecord a = AlternateScanRecord.parseFromBytes(scanRecord);

                           if(a.getServiceUuids().contains(new ParcelUuid(UUID.fromString(HEARTRATE_SERVICE_UUID))))
                           {
                               callBacks.deviceFound(mdv);
                               if(!deviceMap.containsKey(mdv.getAdress()))
                                   deviceMap.put(mdv.getAdress(), mdv);
                               else
                                   Log.d("mioscanner1337", "already contains");
                           }


                        }
                    });
        } else {
            ScanSettings.Builder builder = new ScanSettings.Builder();
            builder.setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY);
            if(mBluetoothAdapter != null && builder != null)
                mBluetoothAdapter.getBluetoothLeScanner().startScan(null, builder.build(), mDeviceFoundCallback = new ScanCallback() {
                    public void onScanResult(int callbackType, ScanResult result) {
                        if (Build.VERSION.SDK_INT > 20)
                        {
                            BleSensor mdv = null;
                            mdv = new BleSensor(result.getDevice());
                            Log.d("1337", "found device: "+mdv.getName());
                        if(mdv != null &&  result.getScanRecord() != null && result.getScanRecord().getServiceUuids() != null && result.getScanRecord().getServiceUuids().contains(new ParcelUuid(UUID.fromString(HEARTRATE_SERVICE_UUID))))
                        {
                            callBacks.deviceFound(mdv);
                            if(!deviceMap.containsKey(mdv.getAdress()))
                                deviceMap.put(mdv.getAdress(), mdv);
                            else
                                Log.d("mioscanner1337", "already contains");
                        }
                        }

                    }

                    /**
                     * Callback when batch results are delivered.
                     *
                     * @param results List of scan results that are previously scanned.
                     */
                    public void onBatchScanResults(List<ScanResult> results) {
                    }

                    /**
                     * Callback when scan could not be started.
                     *
                     * @param errorCode Error code (one of SCAN_FAILED_*) for scan failure.
                     */
                    public void onScanFailed(int errorCode)  {

                    }

                });

        }





    }

    @Override
    public void stopScanning() {
        if (Build.VERSION.SDK_INT < 21) {
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
        } else {
            if(mDeviceFoundCallback != null)
                mBluetoothAdapter.getBluetoothLeScanner().stopScan(mDeviceFoundCallback);

        }

    }


    @Override
    public void connect(HeartRateSensor s) throws HeartRateException{

        if(mBluetoothGatt!=null)
             mBluetoothGatt.disconnect();
        if(mBluetoothGatt != null && mBluetoothGatt.getDevice().getAddress().equals(s.getAdress()))
        {
            Log.d("mio1337", "reconnect");
            // just reconnect
            if(!mBluetoothGatt.connect())
            {
                throw new HeartRateException("Reconnect failed");
            }

        }

        Log.d("mio1337", "normal connect");

        //TODO testen ob s.getBtDevice.connect geht
        BluetoothDevice btDevice = mBluetoothAdapter.getRemoteDevice(s.getAdress());
        mBluetoothGatt = btDevice.connectGatt(mParent, false, btleGattCallback);
        if(mBluetoothGatt == null)
            throw new HeartRateException("Connect failed");

    }


    @Override
    public void close() {

        if(mBluetoothGatt != null)
        {
            mBluetoothGatt.close();
            mBluetoothGatt = null;
        }



    }




    @Override
    public void startMeasurement() throws HeartRateException{
        if(mBluetoothGatt != null)
        {
            List<BluetoothGattService> services = mBluetoothGatt.getServices();
            for (BluetoothGattService service : services) {
                List<BluetoothGattCharacteristic> characteristics = service.getCharacteristics();
                if(service.getUuid().toString().equals(HEARTRATE_SERVICE_UUID))
                {
                    for(BluetoothGattCharacteristic c : characteristics) {

                        Log.d("mio1337characteristic", c.getUuid().toString());
                         if(c.getUuid().toString().equals(HEARTRATE_MEASUREMENT_CHARACTERISTIC_UUID))
                         {
                             List<BluetoothGattDescriptor> descriptors;

                             if((descriptors = c.getDescriptors()) != null && descriptors.size() > 0)
                             {
                                 Log.d("mio1337descriptor", "test");
                                 BluetoothGattDescriptor descriptor = descriptors.get(0);

                                 //Client Characteristic Configuration
                                 if(descriptor.getUuid().toString().equals(CLIENT_CHARACTERISTIC_CONFIGURATION_DESCRIPTOR_UUID))
                                 {

                                     //find descriptor UUID that matches Client Characteristic Configuration (0x2902)
                                     // and then call setValue on that descriptor
                                     Log.d("mio1337descriptor", descriptor.getUuid().toString());

                                     Log.d("mio1337", "descriptorXXX");
                                     //from this exmaple
                                     boolean success = mBluetoothGatt.setCharacteristicNotification(c, true);
                                     if(!success) {
                                         throw new HeartRateException("Setting proper notification status for characteristic failed!");
                                     }

                                     descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);

                                     mBluetoothGatt.writeDescriptor(descriptor);
                                 }

                                 //exception
                                throw new MeasurementException("GATT Descriptor not available");

                             }


                         }
                    }
                }

            }
        }
        //exception



    }



    @Override
    public HeartRateDataSet stopMeasurement() {
        //code for stoping measurement
        running = false;
        if(mBluetoothGatt != null && mBluetoothGattDescriptor != null)
        {

            mBluetoothGattDescriptor.setValue(BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
            mBluetoothGatt.writeDescriptor(mBluetoothGattDescriptor);

        }

        return getHeartRateDataSet();
    }

    @Override
    public void connect(String s) throws HeartRateException {

        if(s!=null && s.length() > 0)
        {
            BleSensor sensor = new BleSensor(s);
            connect(sensor);

        }
        else
            throw new HeartRateException("Can not connect, address invalid or null");
    }


    private final BluetoothGattCallback btleGattCallback = new BluetoothGattCallback() {

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {

            if(timer_started)
            {
                timer_started = false;
                new Thread(new Runnable() {

                    public void run() {
                        while (counter > 0 && running) {
                            try {
                                Thread.sleep(1000);


                            } catch (InterruptedException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }

                            counter--;
                            callBacks.secondDone(counter);
                        }
                        //nur wenn timer abgelaufen
                        if(running)
                        {
                            callBacks.measurementDone(stopMeasurement());
                            running = false;
                        }


                    }

                }).start();
            }

            if(running)
            {
                int type = (characteristic.getValue()[0] & 0x01);

                Date d = new Date();

                int heartRate = Math.abs(characteristic.getValue()[1]);

                Log.d("mioscanner1337", heartRate + " s: "+d.getSeconds());

                HeartRateData tmp = new HeartRateData(heartRate, d.getTime());
                if(callBacks != null)
                    callBacks.newHeartrateData(tmp);
                getHeartRateValues().add(tmp);
            }


        }



        @Override
        public void onConnectionStateChange(final BluetoothGatt gatt, final int status, final int newState) {

            if(status == BluetoothGatt.GATT_SUCCESS)
                Log.d("mioscanner1337", "Gatt success");
            if(gatt != null && deviceMap != null)
            {
                BleSensor mioDevice =  (BleSensor)deviceMap.get(gatt.getDevice().getAddress());
                if(mioDevice == null)
                    mioDevice = new BleSensor(gatt.getDevice());
                if(newState == BluetoothGatt.STATE_CONNECTED)
                {

                    gatt.discoverServices();

                }else if(newState == BluetoothGatt.STATE_DISCONNECTED)
                {
                    running = false;
                    callBacks.connectionStateChanged(mioDevice, CONNECTION.DISCONNECTED);

                    //close();
                    Log.d("mioscanner1337", "Device disconnected "+mioDevice.getName());
                }
            }


        }

        @Override
        public void onServicesDiscovered(final BluetoothGatt gatt, final int status) {

            Log.d("mioscanner1337", "Service discovered");

            String heartRateUuid = "0000180d-0000-1000-8000-00805f9b34fb";
            String heartRateMeasurementUuid = "00002a37-0000-1000-8000-00805f9b34fb";


            List<BluetoothGattService> services = gatt.getServices();
            for (BluetoothGattService service : services) {
                List<BluetoothGattCharacteristic> characteristics = service.getCharacteristics();
                if(service.getUuid().toString().equals(heartRateUuid))
                {
                    for(BluetoothGattCharacteristic c : characteristics) {
                        // if(c.getUuid().toString().equals(heartRateMeasurementUuid))
                        // {
                        Log.d("mioscanner1337", "equals");
                        for (BluetoothGattDescriptor descriptor : c.getDescriptors()) {
                            //find descriptor UUID that matches Client Characteristic Configuration (0x2902)
                            // and then call setValue on that descriptor
                            Log.d("mioscanner1337", "write descriptor");

                            BleSensor mioDevice =  (BleSensor)deviceMap.get(gatt.getDevice().getAddress());
                            if(mioDevice == null)
                                mioDevice = new BleSensor(gatt.getDevice());
                            callBacks.connectionStateChanged(mioDevice, CONNECTION.CONNECTED);
                            //from this exmaple
                            /*boolean success = gatt.setCharacteristicNotification(c, true);
                            if(!success) {
                                Log.e("------", "Seting proper notification status for characteristic failed!");
                            }
                            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                            mBluetoothGattDescriptor = descriptor;*/
                            mBluetoothGatt = gatt;
                            //gatt.writeDescriptor(descriptor);

                            // This is also sometimes required (e.g. for heart rate monitors) to enable notifications/indications
                            // see: https://developer.bluetooth.org/gatt/descriptors/Pages/DescriptorViewer.aspx?u=org.bluetooth.descriptor.gatt.client_characteristic_configuration.xml
                            /*BluetoothGattDescriptor desc = c.getDescriptor(UUID.fromString("00002902-0000-1000-8000-00805f9b34fb"));
                            if(descriptor != null) {
                                byte[] val = true ? BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE : BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE;
                                descriptor.setValue(val);
                                gatt.writeDescriptor(descriptor);
                            }*/
                        }
                        // }
                    }
                }

            }
        }
    };

}
