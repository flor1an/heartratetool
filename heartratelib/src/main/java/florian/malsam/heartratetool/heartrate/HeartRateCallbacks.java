package florian.malsam.heartratetool.heartrate;

/**
 * Created by flo on 18.07.2017.
 */

public interface HeartRateCallbacks {

     void deviceFound(final HeartRateSensor device);
     void connectionStateChanged(HeartRateSensor s, HeartRateManager.CONNECTION state);
     void  measurementDone(HeartRateDataSet s);
     void  newHeartrateData(HeartRateData d);
     void  secondDone(int counter);

    public static class Null implements HeartRateCallbacks {

        @Override
        public void deviceFound(final HeartRateSensor device){

        }

        @Override
        public void connectionStateChanged(HeartRateSensor s, HeartRateManager.CONNECTION state){

        }

        @Override
        public void  measurementDone(HeartRateDataSet s){

        }

        @Override
        public void  secondDone(int counter){

        }

        @Override
        public void newHeartrateData(HeartRateData d) {

        }

    }
}
