package florian.malsam.heartratetool.heartrate;

import android.os.Parcelable;

/**
 * Created by flo on 12.07.2017.
 */

public interface HeartRateSensor extends Parcelable{

    String getAdress();
    String getName();
    void setAdress(String s);
    void setName(String s);
    boolean equals(Object v);
}
