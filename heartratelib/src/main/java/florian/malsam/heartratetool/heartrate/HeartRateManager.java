package florian.malsam.heartratetool.heartrate;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import florian.malsam.heartratetool.heartrate.exception.HeartRateException;


public abstract class HeartRateManager {



    /**
     * enum describing connection state
     */
    public enum CONNECTION{
        CONNECTED, DISCONNECTED, CONNECTING, DISCONNECTING
    }

    //counter variables
    int counter = 1;
    boolean running = false;
    boolean timer_started = false;
    public HeartRateCallbacks callBacks = null;

    /**
     * devices found in scan
     */
    HashMap<String, HeartRateSensor> deviceMap;

    public List<HeartRateData> getHeartRateValues() {
        if(heartRateValues == null)
            return heartRateValues = new ArrayList<>();
        return heartRateValues;
    }

    public void setHeartRateValues(List<HeartRateData> heartRateDatas) {
        this.heartRateValues = heartRateDatas;
    }

    public void clearHeartrateValues()
    {
        if(heartRateValues != null)
            heartRateValues.clear();

    }


    public HeartRateManager(HeartRateCallbacks cb)
    {
        callBacks = cb;
        deviceMap = new HashMap<>();
        heartRateValues = new ArrayList<>();
    }

    public HeartRateManager()
    {

    }

    /**
     * clean list found devices and heartrate measurement
     */
    public void clear() {
        deviceMap = new HashMap<>();
        clearHeartrateValues();
    }

    private List<HeartRateData> heartRateValues;

    public abstract void startScanning();
    public abstract void stopScanning();
    public abstract void initialize() throws HeartRateException;
    public abstract void connect(HeartRateSensor s) throws HeartRateException;
    public abstract void connect(String s) throws HeartRateException;
    public abstract void close();
    public abstract void startMeasurement() throws HeartRateException;
    public abstract HeartRateDataSet stopMeasurement();

    /**
     *
     * @return average beats per minute of measured data
     */
    public int getMeasuredAvg() {

        Log.d("test1337", "size: "+heartRateValues.size());
        if(heartRateValues.size()<1)
            return -1;
        int sum = 0;
        for (HeartRateData v : heartRateValues)
            sum += v.getBpm();

        return sum / heartRateValues.size();
    }




    public int getHighestRate() {
        int highest = 0;
        if(heartRateValues != null)
        {
            for (HeartRateData v : heartRateValues)
                if(v.getBpm() > highest)
                    highest = v.getBpm();

            return highest;
        }
        else return -1;


    }

    public int getLowestRate() {
        int lowest = 1000;
        if(heartRateValues != null) {
            for (HeartRateData v : heartRateValues)
                if(v.getBpm() < lowest)
                    lowest = v.getBpm();
            return lowest;
        } else return -1;


    }

    public void startMeasurement(int seconds) throws HeartRateException {
        counter = seconds;
        running = true;
        timer_started = true;
        startMeasurement();
    }

    public HeartRateDataSet getHeartRateDataSet(){

        HeartRateDataSet set = new HeartRateDataSet();
        set.setHeartRateValues(getHeartRateValues());
        set.setAvgRate(getMeasuredAvg());
        set.setMinRate(getLowestRate());
        set.setMaxRate(getHighestRate());
        if(getHeartRateValues().size() > 1)
        {
            set.setStart(getHeartRateValues().get(0).getTimestamp());
            set.setEnd(getHeartRateValues().get(getHeartRateValues().size()-1).getTimestamp());
        }
        else
        {
            set.setStart(-1);
            set.setEnd(-1);
        }
        return set;
    }




}

