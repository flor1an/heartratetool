package florian.malsam.heartratetool.heartrate;

import android.bluetooth.BluetoothDevice;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by flo on 18.07.2017.
 */

public class BleSensor implements HeartRateSensor {

    private String name;
    private String adress;
    private BluetoothDevice bluetoothDevice;

    public BleSensor(String name)
    {
        setName(name);
    }

    public BleSensor(BluetoothDevice d)
    {
        bluetoothDevice = d;
    }

    private BleSensor(Parcel in)
    {
        adress = in.readString();
        name = in.readString();
        bluetoothDevice = in.readParcelable(BluetoothDevice.class.getClassLoader());
    }

    public static final Parcelable.Creator<BleSensor> CREATOR
            = new Parcelable.Creator<BleSensor>() {
        public BleSensor createFromParcel(Parcel in) {
            return new BleSensor(in);
        }

        public BleSensor[] newArray(int size) {
            return new BleSensor[size];
        }
    };

    @Override
    public String getAdress() {
        if(bluetoothDevice != null)
              return  bluetoothDevice.getAddress();
        else
            return adress;
    }

    @Override
    public String getName() {
        if(bluetoothDevice != null)
            return  bluetoothDevice.getName();
        else
            return name;
    }

    @Override
    public void setAdress(String adress) {
        this.adress = adress;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }



    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof HeartRateSensor))
            return false;
        HeartRateSensor hrs = (HeartRateSensor) obj;
        if(getAdress()!=null && hrs.getAdress()!=null && hrs.getAdress().equals(getAdress()))
            return true;
        return false;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(adress);
        dest.writeString(name);
        dest.writeParcelable(bluetoothDevice, 1);
    }
}
