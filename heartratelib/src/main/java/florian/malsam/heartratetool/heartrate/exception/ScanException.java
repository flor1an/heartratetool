package florian.malsam.heartratetool.heartrate.exception;

/**
 * Created by flo on 04.01.2018.
 */

public class ScanException extends HeartRateException {

    public ScanException(String s)
    {
        super(s);
    }
}
