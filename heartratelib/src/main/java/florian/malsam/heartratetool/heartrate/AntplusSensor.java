package florian.malsam.heartratetool.heartrate;

import android.os.Parcel;

import com.dsi.ant.plugins.antplus.pccbase.AsyncScanController;

/**
 * Created by flo on 18.07.2017.
 */

public class AntplusSensor implements HeartRateSensor {

    private String name;
    private String adress;
    AsyncScanController.AsyncScanResultDeviceInfo antDevice;

    public AntplusSensor(String name)
    {
        setName(name);
    }

    public AntplusSensor(AsyncScanController.AsyncScanResultDeviceInfo device)
    {
        antDevice = device;
    }


    protected AntplusSensor(Parcel in) {
        name = in.readString();
        adress = in.readString();
        antDevice = in.readParcelable(AsyncScanController.AsyncScanResultDeviceInfo.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(antDevice.getAntDeviceNumber()+"");
        dest.writeString(antDevice.getDeviceDisplayName());
        dest.writeParcelable(antDevice, 1);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<AntplusSensor> CREATOR = new Creator<AntplusSensor>() {
        @Override
        public AntplusSensor createFromParcel(Parcel in) {
            return new AntplusSensor(in);
        }

        @Override
        public AntplusSensor[] newArray(int size) {
            return new AntplusSensor[size];
        }
    };

    @Override
    public String getAdress() {
        if(antDevice!=null)

         return ""+antDevice.getAntDeviceNumber();
        else
            return adress;
    }

    @Override
    public String getName() {
        if(antDevice!=null)

            return ""+antDevice.getDeviceDisplayName();
        else
            return name;
    }

    @Override
    public void setAdress(String adress) {
        this.adress = adress;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }


    public Object getRealDevice() {
       return antDevice;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof HeartRateSensor))
            return false;
        HeartRateSensor hrs = (HeartRateSensor) obj;
        if(getAdress()!=null && hrs.getAdress()!=null && hrs.getAdress().equals(getAdress()))
            return true;
        return false;
    }
}
