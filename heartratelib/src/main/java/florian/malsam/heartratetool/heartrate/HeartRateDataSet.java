package florian.malsam.heartratetool.heartrate;

import java.util.List;

/**
 * Created by flo on 07.10.2017.
 */

public class HeartRateDataSet {

    List<HeartRateData> heartRateValues;
    int maxRate;
    int minRate;
    int avgRate;
    long start;
    long end;

    public List<HeartRateData> getHeartRateValues() {
        return heartRateValues;
    }

    public void setHeartRateValues(List<HeartRateData> heartRateValues) {
        this.heartRateValues = heartRateValues;
    }



    public int getMaxRate() {
        return maxRate;
    }

    public void setMaxRate(int maxRate) {
        this.maxRate = maxRate;
    }

    public int getMinRate() {
        return minRate;
    }

    public void setMinRate(int minRate) {
        this.minRate = minRate;
    }

    public int getAvgRate() {
        return avgRate;
    }

    public void setAvgRate(int avgRate) {
        this.avgRate = avgRate;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public long getEnd() {
        return end;
    }

    public void setEnd(long end) {
        this.end = end;
    }

}
