package florian.malsam.heartratetool.heartrate.exception;

/**
 * Created by flo on 04.01.2018.
 */

public class HeartRateException extends Exception {

    public HeartRateException(String s)
    {
        super(s);
    }
}
