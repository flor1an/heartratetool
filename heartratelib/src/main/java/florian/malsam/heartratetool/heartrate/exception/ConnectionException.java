package florian.malsam.heartratetool.heartrate.exception;

/**
 * Created by flo on 04.01.2018.
 */

public class ConnectionException extends HeartRateException {

    public ConnectionException(String s)
    {
        super(s);
    }
}
