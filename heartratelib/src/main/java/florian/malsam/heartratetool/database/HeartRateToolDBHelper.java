package florian.malsam.heartratetool.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by flo on 15.08.2017.
 */

public class HeartRateToolDBHelper extends SQLiteOpenHelper {



    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 3;
    public static final String DATABASE_NAME = "HeartRateTool.db";

    public HeartRateToolDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public void onCreate(SQLiteDatabase db) {

        Log.d("1337", HeartRateToolContract.Measurements.SQL_CREATE_ENTRIES);
        Log.d("1337", HeartRateToolContract.MeasurementData.SQL_CREATE_ENTRIES);
        db.execSQL(HeartRateToolContract.Measurements.SQL_CREATE_ENTRIES);
        db.execSQL(HeartRateToolContract.MeasurementData.SQL_CREATE_ENTRIES);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL(HeartRateToolContract.Measurements.SQL_DELETE_ENTRIES);
        db.execSQL(HeartRateToolContract.MeasurementData.SQL_DELETE_ENTRIES);
        onCreate(db);
    }


}
