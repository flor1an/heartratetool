package florian.malsam.heartratetool.database;

import android.provider.BaseColumns;

/**
 * Created by flo on 15.08.2017.
 */

public final class HeartRateToolContract {

    private HeartRateToolContract() {}

    public static class MeasurementData implements BaseColumns
    {
        public static final String TABLE_NAME = "measurement_data";
        public static final String COLUMN_MEASUREMENTDATA_MEASUREMENTID = "measurement";
        public static final String COLUMN_MEASUREMENTDATA_TIMESTAMP = "timestamp";
        public static final String COLUMN_MEASUREMENTDATA_HEARTRATE = "heartrate";

        public static final String SQL_CREATE_ENTRIES =
                "CREATE TABLE " + MeasurementData.TABLE_NAME + " (" +
                        MeasurementData._ID + " INTEGER PRIMARY KEY," +
                        MeasurementData.COLUMN_MEASUREMENTDATA_MEASUREMENTID + " INTEGER," +
                        MeasurementData.COLUMN_MEASUREMENTDATA_TIMESTAMP + " INTEGER," +
                        MeasurementData.COLUMN_MEASUREMENTDATA_HEARTRATE + " INTEGER," +
                        "FOREIGN KEY("+ MeasurementData.COLUMN_MEASUREMENTDATA_MEASUREMENTID+") REFERENCES "+  Measurements.TABLE_NAME + " (" + Measurements._ID + "))";

        public static final String SQL_DELETE_ENTRIES =
                "DROP TABLE IF EXISTS " + MeasurementData.TABLE_NAME;
    }

    public static class Measurements implements BaseColumns
    {
        public static final String TABLE_NAME = "measurements";
        public static final String COLUMN_NAME_USER = "user";
        public static final String COLUMN_NAME_DEVICE = "device";
        public static final String COLUMN_NAME_SCENARIO = "scenario";
        public static final String COLUMN_NAME_START = "start";
        public static final String COLUMN_NAME_END = "end";
        public static final String COLUMN_NAME_HIGHEST = "highest";
        public static final String COLUMN_NAME_LOWEST = "lowest";
        public static final String COLUMN_NAME_AVG = "average";

        public static final String SQL_CREATE_ENTRIES =
                "CREATE TABLE " + Measurements.TABLE_NAME + " (" +
                        Measurements._ID + " INTEGER PRIMARY KEY," +
                        Measurements.COLUMN_NAME_USER + " TEXT," +
                        Measurements.COLUMN_NAME_DEVICE + " TEXT," +
                        Measurements.COLUMN_NAME_SCENARIO + " TEXT," +
                        Measurements.COLUMN_NAME_START + " INTEGER," +
                        Measurements.COLUMN_NAME_END + " INTEGER," +
                        Measurements.COLUMN_NAME_HIGHEST + " INTEGER," +
                        Measurements.COLUMN_NAME_LOWEST + " INTEGER," +
                        Measurements.COLUMN_NAME_AVG + " INTEGER)";

        public static final String SQL_DELETE_ENTRIES =
                "DROP TABLE IF EXISTS " + Measurements.TABLE_NAME;
    }
}
